# Coding Exercise

#Dependencies

* Swift 2.2: I was not sure you'll have the beta environment setup

No other dependencies (pods or other)

#Build

#### Using xcode: 
Just run the project

#### Using command line:

	xcodebuild -project OVOFlikr2000.xcodeproj -target OVOFlikr2000 build
	
#Testing

#### Using xcode: 
Just run the test target of the project

#### Using command line:
	xcodebuild -project OVOFlikr2000.xcodeproj -scheme OVOFlikr2000 -sdk iphonesimulator test
	
#Todo

Extra things that can be done:

* Pull to refresh (Sorry didn't have time> Southern Rails are to blame :) )...
* More unit tests
* Reload the images from disk when flushed out of memory
* Empty cache on disk when quitting the app
* Add Language Support

