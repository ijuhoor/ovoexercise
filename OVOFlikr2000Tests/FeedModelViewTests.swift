//
//  FeedModelViewTests.swift
//  OVOFlikr2000
//
//  Created by Idriss on 07/08/2016.
//  Copyright © 2016 idriss. All rights reserved.
//

import XCTest

@testable import OVOFlikr2000

class FeedModelViewTests: XCTestCase {

    func testInit() {
        
        let item = self.feedItemWithDate(NSDate())
        let modelView = FeedModelView(item: item)
        
        XCTAssertNotNil(modelView);
    }
    
    func testTitle() {
        
        let item = self.feedItemWithDate(NSDate())
        let modelView = FeedModelView(item: item)
        
        XCTAssertNotNil(modelView);
        XCTAssertEqual(modelView.title(), item.title)
    }
    
    func testDateToday() {

        let item = self.feedItemWithDate(NSDate())
        let modelView = FeedModelView(item: item)
        
        XCTAssertNotNil(modelView);
        XCTAssertEqual(modelView.date(), "Today")
        
    }
    
    func testDateYesterday() {
        
        let item = self.feedItemWithDate(self.relativeDateFrom(NSDate(), relativeDays: -1))
        let modelView = FeedModelView(item: item)
        
        XCTAssertNotNil(modelView);
        XCTAssertEqual(modelView.date(), "Yesterday")
        
    }
        
    private func feedItemWithDate(date: NSDate) -> FeedItem {
        
        return FeedItem(title: "title",
                        mediaURL:NSURL(string: "http://www.google.com")! ,
                        date: date)
        
    }
    
    private func relativeDateFrom(date: NSDate, relativeDays: NSInteger) -> NSDate {
        
        return date.dateByAddingTimeInterval(Double(relativeDays) * 3600 * 24)
        
    }

}
