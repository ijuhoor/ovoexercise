//
//  FeedItemTests.swift
//  OVOFlikr2000
//
//  Created by Idriss on 07/08/2016.
//  Copyright © 2016 idriss. All rights reserved.
//

import XCTest

@testable import OVOFlikr2000

class FeedItemTests: XCTestCase {
    
    func testWithCorrectJSONDict() {
        
        let itemDict = ["title":"a picture",
                        "date_taken":"2016-07-21T02:21:37-08:00",
                        "media":["m":"https://farm9.staticflickr.com/8661/28203206594_fa8febdb81_m.jpg"]];
        
        if let feedItem = FeedItem(with: itemDict) {
            XCTAssertNotNil(feedItem)
        } else {
            XCTFail()
        }
        
    }

    func testMissingMediaData() {
        
        let itemDict = ["title":"a picture",
                        "date_taken":"2016-07-21T02:21:37-08:00"]
        if let _ = FeedItem(with: itemDict) {
            XCTFail()
        } else {
            XCTAssert(true)
        }
        
    }
    
    func testMissingDateData() {
        
        let itemDict = ["title":"a picture",
                        "media":["m":"https://farm9.staticflickr.com/8661/28203206594_fa8febdb81_m.jpg"]]
        if let _ = FeedItem(with: itemDict) {
            XCTFail()
        } else {
            XCTAssert(true)
        }
        
    }
    
    func testMalformedDate() {
        let itemDict = ["title":"a picture",
                        "date_taken":"20160",
                        "media":["m":"https://farm9.staticflickr.com/8661/28203206594_fa8febdb81_m.jpg"]];
        if let _ = FeedItem(with: itemDict) {
            XCTFail()
        } else {
            XCTAssert(true)
        }
    }
}
