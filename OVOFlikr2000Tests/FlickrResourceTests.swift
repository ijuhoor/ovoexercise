//
//  FlickrResourceTests.swift
//  OVOFlikr2000
//
//  Created by Idriss on 07/08/2016.
//  Copyright © 2016 idriss. All rights reserved.
//

import XCTest

@testable import OVOFlikr2000

class FlickrResourceTests: XCTestCase {

    func testResource() {
        
        /* Here we are not trying to test the JSON PArsing but that it the parsing function is executed*/
        
        let feedItemResource = Resource<[FeedItem]>(url: NSURL(string: "http://testURL.com/test")!) { json in
            guard let feedItems = json["items"] as? [JsonDictionary] else { return []}
            return feedItems.flatMap(FeedItem.init)
        }
        
        let path = NSBundle(forClass: FlickrResourceTests.self).pathForResource("testResource", ofType:"json")
        let data = NSData(contentsOfFile: path!)!
        let parsedData = feedItemResource.parse(data)
        
        XCTAssertNotNil(parsedData)
        
    }
    
    func testFailed() {
        
        let feedItemResource = Resource<[FeedItem]>(url: NSURL(string: "http://testURL.com/test")!) { json in
            guard let feedItems = json["test"] as? [JsonDictionary] else { return []}
            return feedItems.flatMap(FeedItem.init)
        }
        
        let path = NSBundle(forClass: FlickrResourceTests.self).pathForResource("testResource", ofType:"json")
        let data = NSData(contentsOfFile: path!)!
        let parsedData = feedItemResource.parse(data)
        
        XCTAssertEqual(parsedData?.count, 0)
    }

}

class FlickrResourceAPITest : XCTestCase {
    
    func testPhotoFeed() {
        let resource = FlickrResourceAPI().photoFeed()
        XCTAssertNotNil(resource)
        
        XCTAssertEqual(resource!.url.absoluteString, "https://api.flickr.com/services/feeds/photos_public.gne?format=json&lang=en-us&nojsoncallback=1")
        
    }
    
    func testPhotoFeedParsing() {
        let resource = FlickrResourceAPI().photoFeed()
        XCTAssertNotNil(resource)
        
        
        let path = NSBundle(forClass: FlickrResourceTests.self).pathForResource("testResource", ofType:"json")
        let data = NSData(contentsOfFile: path!)!
        let parsedData = resource!.parse(data)
        
        XCTAssertNotNil(parsedData)
    }
    
    func testPhotoFeedFailParsing() {
        
        let resource = FlickrResourceAPI().photoFeed()
        XCTAssertNotNil(resource)
        
        
        let path = NSBundle(forClass: FlickrResourceTests.self).pathForResource("malformedResource", ofType:"json")
        let data = NSData(contentsOfFile: path!)!
        let parsedData = resource!.parse(data)
        
        XCTAssertNil(parsedData)
        
    }
    
}
