//
//  FeedModelView.swift
//  OVOFlikr2000
//
//  Created by Idriss on 07/08/2016.
//  Copyright © 2016 idriss. All rights reserved.
//

import Foundation

class FeedModelView {
    
    let item: FeedItem
    
    private let dateFormatter: NSDateFormatter = {
        let dateFormatter = NSDateFormatter()
        dateFormatter.timeStyle = .NoStyle
        dateFormatter.dateStyle = .MediumStyle
        dateFormatter.doesRelativeDateFormatting = true
        return dateFormatter
    }()

    init(item: FeedItem) {
        self.item = item
    }
    
    func title() -> String {
        return item.title
    }
    
    func date() -> String {
        return dateFormatter.stringFromDate(item.date)
    }
    
    
}