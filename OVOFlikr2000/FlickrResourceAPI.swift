//
//  FlickrResourceAPI.swift
//  OVOFlikr2000
//
//  Created by Idriss on 07/08/2016.
//  Copyright © 2016 idriss. All rights reserved.
//

import Foundation

public class FlickrResourceAPI {
    
    let baseURL = NSURL(string: "https://api.flickr.com/services/")
    
    func photoFeed() -> Resource<[FeedItem]>? {
        
        guard let photoFeedURL = NSURL(string: "feeds/photos_public.gne?format=json&lang=en-us&nojsoncallback=1",
                                             relativeToURL: baseURL) else { return nil }
        return Resource<[FeedItem]>(url: photoFeedURL, jsonParser: { json -> [FeedItem]? in
            guard let weatherItems = json["items"] as? [JsonDictionary] else { return nil}
            return weatherItems.flatMap(FeedItem.init)
        })
        
    }
    
}
