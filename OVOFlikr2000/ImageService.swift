//
//  ImageService.swift
//  OVOFlikr2000
//
//  Created by Idriss on 08/08/2016.
//  Copyright © 2016 idriss. All rights reserved.
//

import UIKit

class ImageService {
    
    private var images = [NSURL: UIImage]()
    private var requestedImages = [NSURL:NSURLSessionTask]()
    
    func requestImage(at url:NSURL, onComplete handler:(UIImage?, NSURL)->Void) {
        
        /* we already have the image*/
        if let image = images[url] {
            handler(image, url)
            return
        }

        guard requestedImages[url] == nil else {
            /* the image is already loading */
            return
        }
        
        /* We need to download the image */
        let task = WebService().loadImage(url) { [unowned self] image in

            if let downloadedImage = image {
                self.images[url] = downloadedImage
            }
            handler(image, url)
        }
        
        requestedImages[url] = task
        
    }
    
    func cancelImage(at url:NSURL) {
        if let task = requestedImages[url] {
            task.cancel()
            requestedImages[url] = nil
        }
    }
    
    func reset() {
        images.removeAll()
        requestedImages.removeAll()
    }
    
}

extension WebService {
    
    func loadImage(imageURL: NSURL, completion: (image: UIImage?) -> Void) -> NSURLSessionTask {
        let downloadTask = NSURLSession.sharedSession().downloadTaskWithURL(imageURL,
                                                                            completionHandler: { url, response, error in
                                                                                
                                                                                if let localURL = url,
                                                                                    let data = NSData(contentsOfURL: localURL) {
                                                                                    let image = UIImage(data: data)
                                                                                    dispatch_async(dispatch_get_main_queue(), {
                                                                                        completion(image: image)
                                                                                    })
                                                                                } else {
                                                                                    dispatch_async(dispatch_get_main_queue(), {
                                                                                        completion(image: nil)
                                                                                    })
                                                                                }
                                                                                
                                                                                
        })
        downloadTask.resume()
        return downloadTask
    }
    
}
