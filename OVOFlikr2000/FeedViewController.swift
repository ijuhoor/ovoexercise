//
//  FeedViewController.swift
//  OVOFlikr2000
//
//  Created by Idriss on 07/08/2016.
//  Copyright © 2016 idriss. All rights reserved.
//

import UIKit

enum FeedState {
    case Loading
    case Ready
    case Error
}

enum FeedCellType : String {
    case Normal = "feedCellNormalID"
}


let animationDuration = 0.3

class FeedViewController: UIViewController {

    @IBOutlet var tableView: UITableView!
    @IBOutlet var message: UILabel!
    @IBOutlet var retry: UIButton!
    @IBOutlet var spinner: UIActivityIndicatorView!
    
    private let imageService = ImageService()
    
    private var state: FeedState = .Ready {
        didSet {
            updateState()
        }
    }
    private var feed: [FeedItem] = [] {
        didSet {
            tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
        tableView.delegate   = self
        
        let cellNib = UINib(nibName: "FeedCell", bundle: nil)
        tableView.registerNib(cellNib, forCellReuseIdentifier: cellIDForType(.Normal))

        loadData();
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        imageService.reset()
    }
    
    private func loadData() {
        
        self.state = .Loading
        show("Loading your content")
        
        if let feedResource = FlickrResourceAPI().photoFeed() {
            
            WebService().load(feedResource, completion: { [unowned self] feed, error in
                
                if let feed = feed {
                    
                    self.feed = feed;
                    self.state = .Ready
                    
                } else {
                    
                    self.show(error);
                    self.state = .Error
                }
                
                
            })
            
        } else {
            
            show(nil)
            self.state = .Error
        }
        
    }
    
    
    private func show(error: NSError?) {

        guard error != nil else {
            message.text = "Oops something strange happened."
            return
        }
        
        message.text = error?.localizedDescription
    }
    
    private func show(messageToShow: String) {
        message.text = messageToShow
    }
    
    private func updateState() {
        
        var tableViewAlpha: CGFloat
        var messageAlpha: CGFloat
        var buttonAlpha: CGFloat
        
        if state == .Loading {
            spinner.startAnimating()
            messageAlpha   = 1
            tableViewAlpha = 0
            buttonAlpha    = 0
        } else if state == .Ready {
            spinner.stopAnimating()
            messageAlpha   = 0
            tableViewAlpha = 1
            buttonAlpha    = 0
        } else {
            spinner.stopAnimating()
            messageAlpha   = 1
            tableViewAlpha = 0
            buttonAlpha    = 1
        }
        
        UIView.animateWithDuration(animationDuration) { [unowned self] in
            self.tableView.alpha = tableViewAlpha
            self.message.alpha   = messageAlpha
            self.retry.alpha     = buttonAlpha
        }
    }
    
    @IBAction
    func retryFetching() {
        if state != .Loading {
            self.loadData()
        }
    }
    
    func cellIDForType(type: FeedCellType) -> String {
        return type.rawValue
    }
}


/* Table view Data source*/
extension FeedViewController : UITableViewDataSource {
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return feed.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCellWithIdentifier(cellIDForType(.Normal)) as? FeedCell else { fatalError("cell type not set")}
        
        let item = feed[indexPath.row]
        let formatter = FeedModelView(item: item)
        cell.dateLabel.text = formatter.date()
        cell.titleLabel.text = formatter.title()
        cell.photoView.image = nil
        
        imageService.requestImage(at: item.mediaURL) { image, url in

            if item.mediaURL == url {
                cell.photoView.image = image
            }
        }
        
        return cell
        
    }
    
}

/* Table view delegate*/
extension FeedViewController : UITableViewDelegate {
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return CGFloat(136.0)
    }
    
}
