//
//  FeedItem.swift
//  OVOFlikr2000
//
//  Created by Idriss on 07/08/2016.
//  Copyright © 2016 idriss. All rights reserved.
//

import Foundation

public struct FeedItem {
    public let title    : String
    public let mediaURL : NSURL
    public let date     : NSDate
}

extension FeedItem {
    
    public init?(with jsonDict: [String: AnyObject]) {
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZ" //iso 8601

        guard let title = jsonDict["title"] as? String,
            let url = jsonDict["media"]?["m"] as? String,
            let date = jsonDict["date_taken"] as? String  else { return nil }
        
        if let parsedDate = dateFormatter.dateFromString(date),
            let mediaURL   = NSURL(string: url) {
            self.title = title
            self.mediaURL = mediaURL
            self.date = parsedDate
        } else { return nil }
    }
}